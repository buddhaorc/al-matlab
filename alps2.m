%-- Octave 
pkg load signal;

% Hardware configuration
% Number of sound card inputs
adc_inputs = 2;
% The last input channel is used for latency correction

% read reference signal from file
sound_file = '20kHz_noise2ch.wav';
%sound_file = '4ch.wav';
%sound_file = '/documents/octave/20kHz_noise2ch.wav';
[reference, fs] = audioread (sound_file);
[sz, num] = size (reference);

% Number of outputs should be equal to the number of channals in reference
% sound file
adc_outputs = num;

% Graphs are plotted in 4 rows,
% - output signals
% - input signals
% - correlations of the 1st input with all output signals
% - spectrum of the 1st input and correlation of the correction
%   signal with the 1st output signal.
graph_columns = max(adc_inputs, adc_outputs);  

for i=1:adc_outputs
  subplot (4, graph_columns, i);
  plot (reference(:,i));
end

if (num ~= adc_outputs)
  error ('Sound file %s does not match number of outputs %i\n', sound_file, adc_outputs);
end

duration = 0.4;
blocksize = duration * fs;

% prepare audio I/O
player = audioplayer (reference, fs, 16);
nch = get (player, 'NumberOfChannels');
if (num ~= nch)
  error ('number of channels %i\n', nch);
end

recorder = audiorecorder (fs, 16, adc_inputs);

%-- asynchronous playback and synchronous recording
play (player);
recordblocking (recorder, duration);
stop (player);
stop (recorder);

s = getaudiodata(recorder, 'double');
%-- end of audio input

for i=1:adc_inputs
  subplot (4, graph_columns, graph_columns + i);
  plot (s(:,i));
end

subplot(4, graph_columns, graph_columns * 3 + 1);
fftblock = 4096;
spctr = fft (s(1:fftblock,1));
plot (abs (spctr(1:fftblock/2)));

delays = zeros(num, adc_inputs);
for n = 1:num
  if n == num
    inps = adc_inputs-1;
  else 
    inps = adc_inputs;
  end
  for inp = 1:inps
    correlation = xcorr (reference (1:blocksize, n), s(:,inp));
    correlation = abs (correlation (1:blocksize, 1));
    [ma, idx] = max (correlation);
    delays(n, inp) = idx;
    fprintf('out %i in %i idx %i\n', n, inp, idx);

    if inp == 1
      subplot(4, graph_columns, graph_columns * 2 + n);
      plot (correlation);
    end

    if inp == adc_inputs && n == 1
      subplot(4, graph_columns, graph_columns * 3 + inp);
      plot (correlation);
    end
  end
end

% The last input channel is used for latency correction.
% It is electrically connected to the first output.
md = delays(1, adc_inputs);
delays = (delays - md);
for n = 1:num
  fprintf('speaker %i: ', n);
  for inp = 1:adc_inputs-1
     fprintf('mike %i dist %.2f m ', inp, delays(n, inp));
  end
  fprintf('\n');
end
  
spkLoc = [0,0,0; 0,10,0];
subplot(1,1,1);
loc = sphWaveModelLsTOA(delays(1, adc_inputs - 1), spkLoc, 1000, 1);

