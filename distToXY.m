function [xy] = distToXY( d, L )
% Estimates the location xy from distances to speakers
% located at 0,0 and L,0 
  xy = [0, 0];
  x = (d(1)^2 + L^2 - d(2)^2) / 2 / L;
  if d(1) >= x 
    xy(1) = x;
    xy(2) = sqrt(d(1)^2 - x^2);
  end